//Modify this file to change what commands output to your statusbar, and recompile using the make command.

// T = To update when waking up
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	// T
	{" ","updates",                                         300,            1},
	{"\uf85a ", "free -h | awk '/^Mem/ { print $3\"/\"$2 }' | sed s/i//g",	30,		0},

	{" ","light",                                          0,              12},

	{"", "volume",						0,		10},

	// T
	{"", "battery",						30,		2},

	// T
	{"", "date '+%R'",					5,		3},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " | ";
static unsigned int delimLen = 5;
