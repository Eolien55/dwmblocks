//Modify this file to change what commands output to your statusbar, and recompile using the make command.

#define GREEN		"2"
#define CYAN		"6"
#define LRED		"9"
#define BLUE		"4"
#define YELLOW		"3"
#define LYELLOW		"11"
#define LMAGENTA 	"13"
#define LORANGE		"17"

#define ACCENT_BG	"18"
#define ACCENT_FG	"19"

// T = To update when waking up
static const Block blocks[] = {
	/*Icon*/			/*Command*/		/*Update Interval*/	/*Update Signal*/
	{"^C"LMAGENTA"^",		"mod music",		0,			13}, 
	{"",				"tsc",			0,			14},
	// T
	{"^C"GREEN"^ ",		"mod updates",		300,			1},
	// T
	{"^C"LORANGE"^ ",		"mod rss",		300,			4},
	{"^C"CYAN"^ ",			"mod memory",		30,			11},
	{"^C"BLUE"^ ",			"mod light",		0,			12},
	{"^C"LRED"^",			"mod volume",		0,			10},
	// T
	{"^C"YELLOW"^",			"mod battery",		30,			2},
	// T
	{"",				"mod date",		5,			3},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = "^d^ ";
static unsigned int delimLen = 5;
